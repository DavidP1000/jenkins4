FROM node:19.6.0-bullseye
EXPOSE 8000
COPY . .
RUN npm install
CMD npm start

